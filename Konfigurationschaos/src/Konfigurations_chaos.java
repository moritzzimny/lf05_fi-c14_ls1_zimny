
public class Konfigurations_chaos {
	
	public static void main(String[] args) {
		
		String name;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		int euro;
		int cent;
		int summe;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		double fuellstand;
		double maximum = 100.00;
		double patrone = 46.24;
		char sprachModul = 'd';
		final byte PRUEFNR = 4;
		boolean statusCheck;
		fuellstand = maximum - patrone;
		name = typ + " " + bezeichnung;
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;
		statusCheck = (euro <= 150) 
		&& (euro >= 50)
		&& (cent != 0)
		&& (sprachModul == 'd')
		&& (fuellstand >= 50.00) 
		&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + statusCheck);
	
	}

}

//#/
//{
//{
//}
//}
//Konfiguration_Chaos 	
//public class 
//public static void 
//int
//int
//int
//double
//String 
//char
//boolean
//String 
//String
//final
//byte
//double
//double
//int
//int
//name = typ + " " + bezeichnung;
//name;
//typ = "Automat AVR";
//bezeichnung = "Q2021_FAB_A";
//sprachModul = 'd';
//PRUEFNR = 4;
//fuellstand;
//maximum = 100.00;
//patrone = 46.24;
//fuellstand = maximum - patrone;
//euro = summe / 100;
//euro;
//cent;
//cent = summe % 100;
//summe;
//summe = muenzenCent + muenzenEuro * 100;
//muenzenCent = 1280;
//muenzenEuro = 130;
//&& (fuellstand >= 50.00) 
//&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
//statusCheck;
//statusCheck = (euro <= 150) 
//&& (euro >= 50)
//&& (cent != 0)
//&& (sprachModul == 'd')
//System.out.println("Name: " + name);
//System.out.println("Sprache: " + sprachModul);
//System.out.println("Pr�fnummer : " + PRUEFNR);
//System.out.println("F�llstand Patrone: " + fuellstand + " %");
//System.out.println("Summe Euro: " + euro +  " Euro");
//System.out.println("Summe Rest: " + cent +  " Cent");		
//System.out.println("Status: " + statusCheck);
//main(String[] args) 



