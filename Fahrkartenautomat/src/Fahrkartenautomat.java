﻿// der richtige fahr krten automart

import java.util.Scanner;

public class Fahrkartenautomat{
	static Scanner tastatur = new Scanner(System.in);
	static int anzahlTickets;
	
	public static void main(String[] args) {		
		while(true) {
			//die while schleife ist da um das program 24/7 am laufend zu halten weil es ja auch so bei einen 
			//echten ist es wollen ja mehr leute ein ticket kaufen den System out unter dem kommentar habe ich 
			//hinzugefügt aus hoefflichkeit und als marker das man shiet das er wieder bereit ist 
			
			System.out.println("Willkommen beim beklopten Fahrkartenautomart"
					+ "\n");
			boolean p= true;

			double zuZahlenderBetrag = 0.0;
		
			while(p) {
			// System.out.println(111);
				zuZahlenderBetrag += fahrkartenbestellungErfassen();
				System.out.println("\n"
						+ "\n******************************************************************************"
						+ "\n!!!möchten sie weitere tickets kaufen!!! "
						+ "\n************************************************************************************* "
						+ "\n [!!!Antworten sie bitte mit ja oder nein!!!]");
			String eingabe = tastatur.next();
			
			 switch(eingabe){
				 case "ja" : System.out.println("Sie kaufen weiter ein");
					 break;
				 case "nein": p =false; System.out.println("Sie gehen zur kasse");
					 break;
					 default: p= false; System.out.println("Fehlerhafte eingabe sie werden zur kasse weiter geleitet");
					 
			 }
			}
			
			double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n" + "Wir wünschen Ihnen eine gute Fahrt.");
			System.out.println("  \n \n \n \n \n \n \n \n ");
		}
	
	}

	public static double fahrkartenbestellungErfassen() {
		int[] nummer = {1,2, 3,4,5,6,7,8,9,10};
		double[] preisliste = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
			
	
		String[] ticketname = {"[Einzelfahrschein Berlin AB]" ,
				"[Einzelfahrschein Berlin BC]",
				"[Einzelfahrschein Berlin ABC]",
				"[Kurzstrecke]",
				"[Tageskarte Berlin AB]",
				"[Tageskarte Berlin BC]",
				"[Tageskarte Berlin ABC]",
				"[Kleingruppen-Tageskarte Berlin AB]",
				"[Kleingruppen-Tageskarte Berlin BC]",
				"[Kleingruppen-Tageskarte Berlin ABC]",} ;
		
			for(int i = 0;i < nummer.length; i++) {
				System.out.printf("%d\t%.2f\t%s\n", nummer[i], preisliste[i], ticketname[i]);
			}
			
	
	
		System.out.println("\n !!!Bitte wählen sie ihren Tarief!!!\n");
		int auswahl = tastatur.nextInt();
		auswahl -= 1;
		
		System.out.printf("[IHRE WAHL:]\n"
				+ "%d\t%.2f\t%s\n", nummer[auswahl], preisliste[auswahl], ticketname[auswahl]);
	
			
		
		System.out.printf("Ticketpreis (Euro): %.2f\n", preisliste[auswahl]);
		System.out.print("!!!!Anzahl der Tickets: !!!! ");
		anzahlTickets = tastatur.nextInt();
		if(anzahlTickets > 10 || anzahlTickets < 1 ) {
			System.out.println("Sie können maximal 10 Tickets aufeinmal kaufen sie Fahrenfort mit einem ticket");
			return  preisliste[auswahl] * 1;
		}
		
		return  preisliste[auswahl] * anzahlTickets;
	
		}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, hoechstens 50 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	public static void fahrkartenAusgeben() {
		// Aenderung von einzahl zu mehrzahl
		if( anzahlTickets > 1)
			System.out.println("\n Fahrscheine werden ausgegeben");	
		else
			System.out.println("\n Fahrschein wird ausgegeben");
			
		for (int i = 0; i < 8; i++) {
	    	System.out.print("=");
	        try {
	        	Thread.sleep(250);
	        } catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
		System.out.println("\n\n");
	} 
	
	public static void rueckgeldAusgeben(double rückgabebetrag){
		if(rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen und Geldscheinen ausgezahlt:");

			while(rückgabebetrag >= 50.0) { //  50 EURO-Schein
				System.out.println("50 EURO");
				
				System.out.println("*********************");
				System.out.println("*  					*");
				System.out.println("* 	50 Euro  50	    *");
				System.out.println("* 					*");
				System.out.println("*********************");
				rückgabebetrag -= 50.0;

				
			}
			while(rückgabebetrag >= 20.0) { // 20 EURO-Schein
				System.out.println("20 EURO");
				
				System.out.println("*********************");
				System.out.println("*  					*");
				System.out.println("* 	20 Euro  20	    *");
				System.out.println("* 					*");
				System.out.println("*********************");
				rückgabebetrag -= 20.0;
			
			}
			while(rückgabebetrag >= 10.0) { // 10 EURO-Schein
				System.out.println("10 EURO");
				
				System.out.println("*********************");
				System.out.println("*                   *");
				System.out.println("* 	10 Euro  10	    *");
				System.out.println("*                   *");
				System.out.println("*********************");
				rückgabebetrag -= 10.0;
			}
			while(rückgabebetrag >= 5.0) { // 5 EURO-Schein
				System.out.println("5 EURO");
				
				System.out.println("*********************");
				System.out.println("*  					*");
				System.out.println("* 	5  Euro  5	    *");
				System.out.println("* 					*");
				System.out.println("*********************");
				rückgabebetrag -= 5.0;
			}
			while(rückgabebetrag >= 2.0) { // 2 EURO-Münzen 
				System.out.println("2 EURO");
				
				System.out.println("      *****    ");
				System.out.println("     *  2  *    ");
				System.out.println("    * Euro  *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****    ");
				rückgabebetrag -= 2.0;
			}
			while(rückgabebetrag >= 1.0) { // 1 EURO-Münzen
				System.out.println("1 EURO");
				
				System.out.println("      *****    ");
				System.out.println("     *  1  *    ");
				System.out.println("    * Euro  *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****    ");
				rückgabebetrag -= 1.0;
			}
			while(rückgabebetrag >= 0.5) { // 50 CENT-Münzen
				System.out.println("50 CENT");
				
				System.out.println("      *****     ");
				System.out.println("     * 50  *    ");
				System.out.println("    *  Cent *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****    ");
				rückgabebetrag -= 0.5;
			}
			while(rückgabebetrag >= 0.2) { // 20 CENT-Münzen
				System.out.println("20 CENT");
				
				System.out.println("      *****     ");
				System.out.println("     * 20  *    ");
				System.out.println("    *  Cent *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****     ");
				rückgabebetrag -= 0.2;
			}
			while(rückgabebetrag >= 0.1) { // 10 CENT-Münzen
				System.out.println("10 CENT");

				System.out.println("      *****     ");
				System.out.println("     * 10  *    ");
				System.out.println("    *  Cent *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****    ");
				rückgabebetrag -= 0.1;
			}
			while(rückgabebetrag >= 0.05) { // 5 CENT-Münzen
				System.out.println("5 CENT");
				

				System.out.println("      *****     ");
				System.out.println("     *  5  *    ");
				System.out.println("    *  Cent *   ");
				System.out.println("     *     *    ");
				System.out.println("      *****    ");
				rückgabebetrag -= 0.05;
			}
			
		}
		
	}
}