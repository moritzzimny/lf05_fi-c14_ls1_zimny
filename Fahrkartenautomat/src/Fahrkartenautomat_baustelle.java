// der richtige fahr krten automart

import java.util.Scanner;

public class Fahrkartenautomat_baustelle {
	static Scanner tastatur = new Scanner(System.in);
	static int anzahlTickets;
	
	public static void main(String[] args) {		
		while(true) {
			//die while schleife ist da um das program 24/7 am laufend zu halten weil es ja auch so bei einen 
			//echten ist es wollen ja mehr leute ein ticket kaufen den System out unter dem kommentar habe ich 
			//hinzugef�gt aus höfflichkeit und als marker das man shiet das er wieder bereit ist 
			
			System.out.println("Willkommen beim beklopten fahrkartenautomart"
					+ "\n");
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(r�ckgabebetrag);
			
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n" + "Wir w�nschen Ihnen eine gute Fahrt.");
			System.out.println("  \n \n \n \n \n \n \n \n ");
		}
	
	}
	
	public static double fahrkartenbestellungErfassen() {
		int[] nummer = {1,2, 3,4,5,6,7,8,9,10};
		double[] preisliste = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		
		String[] ticketname = {"Einzelfahrschein Berlin AB" ,"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
				"Tageskarte Berlin AB",
				"Tageskarte Berlin BC",
				"Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC",} ;
	
		
		
		for(int i = 0;i < nummer.length; i++) {
			System.out.printf("%d\t%.2f\t%s\n", nummer[i], preisliste[i], ticketname[i]);
		}
	
		System.out.println("\n Bitte wählen sie ihren Tarief\n");
		int auswahl = tastatur.nextInt();
		auswahl -= 1;
		
		System.out.printf("IHRE WAHL:\n"
				+ "%d\t%.2f\t%s\n", nummer[auswahl], preisliste[auswahl], ticketname[auswahl]);
		
			
		
		System.out.printf("Ticketpreis (Euro): %.2f\n", preisliste[auswahl]);
		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();
		if(anzahlTickets > 10 || anzahlTickets < 1 ) {
			System.out.println("Sie können maximal 10 Tickets aufeinmal kaufen sie Fahrenfort mit einem ticket");
			return  preisliste[auswahl] * 1;
		}
		
		return  preisliste[auswahl] * anzahlTickets;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	public static void fahrkartenAusgeben() {
		// änderung von einzahl zu mehrzahl
		if( anzahlTickets > 1)
			System.out.println("\n Fahrscheine werden ausgegeben");	
		else
			System.out.println("\n Fahrschein wird ausgegeben");
			
		for (int i = 0; i < 8; i++) {
	    	System.out.print("=");
	        try {
	        	Thread.sleep(250);
	        } catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
		System.out.println("\n\n");
	} 
	
	public static void rueckgeldAusgeben(double r�ckgabebetrag){
		if(r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in Höhe von %.2f Euro\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");
			
			while(r�ckgabebetrag >= 2.0) { // 2 EURO-M�nzen 
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while(r�ckgabebetrag >= 1.0) { // 1 EURO-M�nzen
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while(r�ckgabebetrag >= 0.5) { // 50 CENT-M�nzen
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while(r�ckgabebetrag >= 0.2) { // 20 CENT-M�nzen
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while(r�ckgabebetrag >= 0.1) { // 10 CENT-M�nzen
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while(r�ckgabebetrag >= 0.05) { // 5 CENT-M�nzen
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}
}