
public class Ladung {

	public String bezeichung;
	public int menge;

	@Override
	public String toString() {
		return "Ladung [bezeichung=" + bezeichung + ", menge=" + menge + "]";
	}

	public Ladung() {
		this.bezeichung = "0";
		this.menge = 0;
	}

	public Ladung(int m, String b) {
		this.bezeichung = b;
		this.menge = m;

	}

	public String getbezeichung() {
		return this.bezeichung;
	}

	public void setbezeichung(String b) {
		this.bezeichung = b;
	}

//------------------------------------------------------------------------------------------------------------
	public int getmenge() {
		return this.menge;
	}

	public void setmenge(int m) {
		this.menge = m;
	}

	

}
