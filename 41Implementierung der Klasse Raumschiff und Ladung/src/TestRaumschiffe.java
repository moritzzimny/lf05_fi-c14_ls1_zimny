
public class TestRaumschiffe {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni Baumkuchen", 5);
		Ladung Ladung1 = new Ladung(200, "Ferengi Schneckensaft");
		Ladung Ladung2 = new Ladung(5,"Borg-Schrott");
		Ladung Ladung3 = new Ladung(2, "Rote Materie");
		Ladung Ladung4 = new Ladung(35, "Forschungssonde");
		Ladung Ladung5 = new Ladung(200, "Bat'leth Klingonen Schwert");
		Ladung Ladung6 = new Ladung(50, "Plasma-Waffe");
		Ladung Ladung7 = new Ladung(3,"Photonentorpedo");
		
		
		
		klingonen.addLadung(Ladung1);
		romulaner.addLadung(Ladung2);
		romulaner.addLadung(Ladung3);
		vulkanier.addLadung(Ladung4);
		klingonen.addLadung(Ladung5);
		vulkanier.addLadung(Ladung6);
		romulaner.addLadung(Ladung7);
		
		klingonen.raumschiffZustnd();
		klingonen.ladungsinhalt();
		
		romulaner.raumschiffZustnd();
		romulaner.ladungsinhalt();
		
		vulkanier.raumschiffZustnd();
		vulkanier.ladungsinhalt();
		
	}
}
