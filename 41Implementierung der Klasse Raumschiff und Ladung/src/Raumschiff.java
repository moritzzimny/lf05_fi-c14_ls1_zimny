import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedo;
	private int energieVersogungInProzent;
	private int schieldeInProzent;
	private int hüllenintigrätätInProzent;
	private int lebenserhaltungssysteminProzent;
	private String schiffsname;
	private int androidenanzahl;
	private ArrayList<Ladung> Landungsverzeichnis;
	private ArrayList<String> brodcastkomnikator;

	public Raumschiff() {
		this.photonentorpedo = 0;
		this.energieVersogungInProzent = 0;
		this.schieldeInProzent = 0;
		this.hüllenintigrätätInProzent = 0;
		this.lebenserhaltungssysteminProzent = 0;
		this.schiffsname = "0";
		this.androidenanzahl = 0;
		this.Landungsverzeichnis = new ArrayList<Ladung>();
		this.brodcastkomnikator = new ArrayList<String>();
	}

	public Raumschiff(int pt, int evp, int sp, int hint, int leben, String name, int an) {
		this.photonentorpedo = pt;
		this.energieVersogungInProzent = evp;
		this.schieldeInProzent = sp;
		this.hüllenintigrätätInProzent = hint;
		this.lebenserhaltungssysteminProzent = leben;
		this.schiffsname = name;
		this.androidenanzahl = an;
		this.Landungsverzeichnis = new ArrayList<Ladung>();
		this.brodcastkomnikator = new ArrayList<String>();
	}

	@Override
	public String toString() {
		return "Raumschiff [photonentorpedo=" + photonentorpedo + ", energieVersogungInProzent="
				+ energieVersogungInProzent + ", schieldeInProzent=" + schieldeInProzent
				+ ", hüllenintigrätätInProzent=" + hüllenintigrätätInProzent + ", lebenserhaltungssysteminProzent="
				+ lebenserhaltungssysteminProzent + ", schiffsname=" + schiffsname + ", androidenanzahl="
				+ androidenanzahl + ", Landungsverzeichnis=" + Landungsverzeichnis + "]";
	}

	public int getphotonentorpedo() {
		return this.photonentorpedo;
	}

	public void setphotonentorpedo(int pt) {
		this.photonentorpedo = pt;
	}

//--------------------------------------------------------------------------------------------------------
	public int getenergieVersogungInProzent() {
		return this.energieVersogungInProzent;
	}

	public void setenergieVersogungInProzent(int evp) {
		this.energieVersogungInProzent = evp;
	}

	public int getSchieldeInProzent() {
		return schieldeInProzent;
	}

	public void setSchieldeInProzent(int schieldeInProzent) {
		this.schieldeInProzent = schieldeInProzent;
	}

	public int getHüllenintigrätätInProzent() {
		return hüllenintigrätätInProzent;
	}

	public void setHüllenintigrätätInProzent(int hüllenintigrätätInProzent) {
		this.hüllenintigrätätInProzent = hüllenintigrätätInProzent;
	}

	public int getLebenserhaltungssysteminProzent() {
		return lebenserhaltungssysteminProzent;
	}

	public void setLebenserhaltungssysteminProzent(int lebenserhaltungssysteminProzent) {
		this.lebenserhaltungssysteminProzent = lebenserhaltungssysteminProzent;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getAndroidenanzahl() {
		return androidenanzahl;
	}

	public void setAndroidenanzahl(int androidenanzahl) {
		this.androidenanzahl = androidenanzahl;
	}

	public void addLadung(Ladung neueLadung) {
		this.Landungsverzeichnis.add(neueLadung);
	}

	public void raumschiffZustnd() {
		System.out.println("Name des Schiffes " + this.schiffsname);
		System.out.println("################################################");
		System.out.println("Anzahl der Topedos " + this.photonentorpedo);
		System.out.println("Energi " + this.energieVersogungInProzent + "%");
		System.out.println("Schield " + this.schieldeInProzent + "%");
		System.out.println("Schiffs Hüllen Zustand " + this.hüllenintigrätätInProzent + "%");
		System.out.println("Zustand der Lebenserhaltung " + this.lebenserhaltungssysteminProzent + "%");
		System.out.println("Das schiff hat so viele androiden am bord " + this.androidenanzahl);
		System.out.println(
				"_________________________________________________________________________________________________");
	}

	public void ladungsinhalt() {
		System.out.println("Die gesamte Ladung " + this.Landungsverzeichnis);
		System.out.println("################################################################");
	}

	public void nachrichtAnAlle(String a) {
		System.out.println("Nachricht an Alle:" + a);
		this.brodcastkomnikator.add("\n" + a);

		System.out.println("(---Click---)");
	}

	public void topedo() {
		System.out.println("Topedo werden vorbereitet");
		if (this.photonentorpedo < 1) {
			this.nachrichtAnAlle("(---Click---)");
		} else {
			System.out.println("Topedo wird abgeschossen");
			this.photonentorpedo = this.photonentorpedo - 1;
		}
	}

	public void kanonen() {
		System.out.println("Kanonen werden vorbereitet");
		if (this.energieVersogungInProzent < 50) {
			this.nachrichtAnAlle("(---Click---)");
		} else {
			System.out.println("Kanonen feuern!!!!!!!!!!!!!!");
			this.energieVersogungInProzent = this.energieVersogungInProzent - 50;
		}
	}

	public void treffer(Raumschiff ziel) {
		this.nachrichtAnAlle("" + ziel.getSchiffsname() + "wurde getrofen");
		ziel.trefferVermerkken();
	}

	public void trefferVermerkken() {
		this.schieldeInProzent = this.schieldeInProzent - 50;
		if(this.schieldeInProzent==0) {
			this.hüllenintigrätätInProzent=this.hüllenintigrätätInProzent-50;
			
		}
	}
}
