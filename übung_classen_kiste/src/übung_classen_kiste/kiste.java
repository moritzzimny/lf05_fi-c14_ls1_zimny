package �bung_classen_kiste;

public class kiste {

	public int h�he;
	public int breite;
	public int tiefe;
	public String farbe;
	public int volumen;
	public kiste() {
		this.farbe = "gr�n";
		this.breite = 10;
		this.h�he = 10;
		this.tiefe = 10;
	}

	public kiste(int h, int b, int t, String f) {
		this.h�he = h;
		this.breite = b;
		this.tiefe = t;
		this.farbe = f;
	}

	public int getH�he() {
		return h�he;
	}

	public void setH�he(int h�he) {
		this.h�he = h�he;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getTiefe() {
		return tiefe;
	}

	public void setTiefe(int tiefe) {
		this.tiefe = tiefe;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	public int volumen() {
		volumen=this.h�he*this.breite*this.tiefe;
		return volumen;
	}
}
