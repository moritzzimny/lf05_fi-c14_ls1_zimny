
import java.util.Scanner; // Import der Klasse Scanner

public class Rechner {

	public static void main(String[] args) // Hier startet das Programm
	{

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		double zahl1 = myScanner.nextInt();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		double zahl2 = myScanner.nextInt();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		double ergebnis = zahl1 + zahl2;
		double ergebnis1 = zahl1 - zahl2;
		double ergebnis2 = zahl1 * zahl2;
		double ergebnis3 = zahl1 / zahl2;
		System.out.println("\n\n\nErgebnis der Grundrechenarten lautet: ");
		System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
		System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnis1);
		System.out.println(zahl1 + " x " + zahl2 + " = " + ergebnis2);
		System.out.println(zahl1 + " : " + zahl2 + " = " + ergebnis3);

		myScanner.close();

	}
}
